// Version of the cach
const cacheName = "v65446565656546541";

// List of pages we want to put in the cach
const cacheAssets = [
    'index.html',
    'about.html',
    "css/style.css",
    "js/main.js"
];

/*
*   Trigger the install Event
*/
self.addEventListener('install', (event) => {
    console.log("Service Worker: installed");
    event.waitUntil(
        caches
        .open(cacheName)
        .then(cache => {
            console.log("Service Worker: caching files");
            cache.addAll(cacheAssets);
        })
        .then(() => self.skipWaiting())
    );
})

/*
*   Trigger the activate event
*/
self.addEventListener('activate', (event) => {
    console.log("Service Worker: activated");
    //Remove unwanted caches
    event.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames.map(cache => {
                    if (cache !== cacheName) {
                        console.log(`Service Worker: clearing ${cacheNames}`);
                        return caches.delete(cache);
                    }
                })
            )
        })
    );
});


/*
*   Trigger fetch event (offline mode)
*/
self.addEventListener('fetch', event => {
    console.log("Service Worker: fetching");
    event.respondWith(fetch(event.request).catch(() => caches.match(event.request)));
});