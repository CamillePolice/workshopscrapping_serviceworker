/**
 * Debug mode status
 */
debugMode = true;

/**
 * Name of the current cache
 */
var cacheName = 'TestCache';

/**
 * Static files to cache
 */
var cacheFiles = [
    'index.html',
    'about.html'
]

/**
 * Triggered at the installation of the SW on the browser
 * This event stores 'cacheFiles' in the cached
 */
self.addEventListener('install', function (event) {
    displayMessage("[ServiceWorker] > [Installation in progress]");
    event.waitUntil(caches.open(cacheName).then(addFilesInCache).catch("ServiceWorker > Caching files failed"));
    return self.skipWaiting();
});

/**
 * Triggered at the activation of the SW
 * This event clears all the caches which aren't the new activated SW
 */
self.addEventListener('activate', function (event) {
    displayMessage("[ServiceWorker] > [Activation in progress]");
    event.waitUntil(caches.keys().then(deleteExistingCaches));
})

/**
 * Trigerred each time a page included in the SW is called
 * This event will use a cache request if it exists or it will cache the new request
 */
self.addEventListener('fetch', function (event) {
    event.respondWith(
        caches.open(cacheName).then(function (cache) {
            return cache.match(event.request)
                .then(function (response) {
                    return response || fetch(event.request).then(function (response) {
                        cache.put(event.request, response.clone());
                        displayMessage("[ServiceWorker] > [Data cached]: " + event.request.url);
                        return response;
                    });
                })
                .catch(errorTriggered("ServiceWorker > Error Fetching & Caching new Datas"));
        })
    );
});

/**
 * Private function
 */
function errorTriggered(text, error) {
    if (debugMode)
        console.log("[" + text + "]: " + error);
}

function displayMessage(text) {
    if (debugMode)
        console.log(text);
}

function addFilesInCache(cache) {
    displayMessage("[ServiceWorker] > [Installed]");
    displayMessage("[ServiceWorker] > [Caching files]");
    return cache.addAll(cacheFiles);
}

function deleteExistingCaches(keyList) {
    return Promise.all(keyList.map(function (key) {
        if (cacheName.indexOf(key) === -1) {
            displayMessage("[ServiceWorker] > [Removing old caches]: " + key);
            return caches.delete(key);
        }
    }));
}
